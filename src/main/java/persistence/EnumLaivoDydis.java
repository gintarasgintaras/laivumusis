package persistence;

public enum EnumLaivoDydis {
    KETURVIETIS(1),
    TRIVIETIS(2),
    DVIVIETIS(3),
    VIENVIETIS(4);
    public final int kiekis;
    EnumLaivoDydis(int kiekis){
        this.kiekis=kiekis;
    }
}
