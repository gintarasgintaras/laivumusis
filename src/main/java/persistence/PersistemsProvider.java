package persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistemsProvider {
    private EntityManagerFactory sessionFactory;
    private EntityManager entityManager;
    public PersistemsProvider() {
        sessionFactory = Persistence.createEntityManagerFactory( "org.hibernate.tutorial.jpa" );
        entityManager=sessionFactory.createEntityManager();
    }

    public EntityManagerFactory getSessionFactory() {
        return sessionFactory;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
