import persistence.EnumLaivoDydis;
import persistence.JDBCImplementation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class App {
    public static void main(String[] args) {
        ResultSet resultSet;
        JDBCImplementation jdbc=new JDBCImplementation();
        Connection connection = jdbc.getConnection();

        try {
            resultSet = connection.createStatement().executeQuery("select * from pirma");
            while (resultSet.next()){
                System.out.print(resultSet.getInt("amzius")+" ");
                System.out.println(resultSet.getString("vardas")+" ");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            int kiekIrasuGrazina = connection.createStatement().executeUpdate("update pirma set amzius=amzius+50 where vardas like '%as%' ");
                System.out.println(kiekIrasuGrazina);
            System.out.println(EnumLaivoDydis.KETURVIETIS);

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
